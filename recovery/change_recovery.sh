#!/sbin/sh
BUSYBOX="/tmp/busybox"
$BUSYBOX tar -C / -zxvf /tmp/rec.tgz
pid=$($BUSYBOX pidof recovery)
$BUSYBOX kill $pid
