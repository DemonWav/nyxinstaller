#!/sbin/sh
# Installer script for Team Nyx
# © 2012 Team Nyx - DemonWav
# Written by DemonWav (Kyle Wood)

# This script is part of the Team Nyx AROMA Installer, all credits
# for the AROMA binary, as well as the source code and documentation
# of the binary go to amarullz. Big thanks to him for the great help
# he was in this whole process.

# This file is part of the Team Nyx AROPMA Installer.
# The installer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# The installer is distributed in hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details. See
# <http://www.gnu.org/licenses/> for more information.

set -x
PATH=$PATH:/tmp:/tmp/updates:/tmp/teamnyx:/sbin

BUSYBOX="/tmp/teamnyx/busybox"

mount_part()
{
	if ! [ $($BUSYBOX mount | $BUSYBOX grep "/$1" | $BUSYBOX wc -l) -eq "1" ] ; then
		$BUSYBOX mkdir -p /$1
		$BUSYBOX umount -l /dev/block/$2
		if ! $BUSYBOX mount -t $3 /dev/block/$2 /$1 ; then
			echo "$1 cannot be mounted successfully."
		fi
	else
		$BUSYBOX umount -l /dev/block/$2
		if ! $BUSYBOX mount -t $3 /dev/block/$2 /$1 ; then
			echo "$1 cannot be mounted successfully."
		fi
	fi
}

format_data()
{
	mount_sdcard
	exec >> /tmp/format_data.log 2>&1
	#mount system
	mount_part system mtdblock2 yaffs2
	#copy needed libs for mkfs.ext4
	$BUSYBOX cp -r /tmp/updates/lib/* /system/lib/
	#make needed symlink for mkfs.ext4
	$BUSYBOX ln -s /tmp/updates/mke2fs /tmp/updates/mkfs.ext4
	ext4_format mmcblk0p2
	mount_new data mmcblk0p2
}

ext4_format()
{
	/tmp/updates/mkfs.ext4 -F -J size=32 -T default /dev/block/$1
	/tmp/updates/tune2fs -c 100 -i 100d -m 0 /dev/block/$1
}

mount_new()
{
	/tmp/updates/e2fsck -p /dev/block/$2
	$BUSYBOX mount -t ext4 -o noatime,barrier=0,data=ordered,barrier=1,noauto_da_alloc /dev/block/$2 /$1
}

wipedata()
{
	mount_sdcard
	exec >> /tmp/wipedata.log 2>&1
	#format cache
	$BUSYBOX umount -l /cache
	/tmp/erase_image cache
	#format data
	$BUSYBOX umount /data
	format_data
	#format datadata
	$BUSYBOX umount -l /datadata
	/tmp/erase_image datadata
	#cleanup sdcard
	$BUSYBOX rm -rf /sdcard/.android_secure
	$BUSYBOX umount -l /data
}

partitionchecker()
{
	mount_sdcard
	exec >> /tmp/partitionchecker.log 2>&1
	if [ $($BUSYBOX cat /proc/partitions | $BUSYBOX grep "mmcblk0p2" | $BUSYBOX wc -l) != 1 ] ; then
		TYPE=CDMA
	elif [ $($BUSYBOX cat /proc/partitions | $BUSYBOX grep "mtdblock2" | $BUSYBOX grep "256000" | $BUSYBOX wc -l) == 1 ] ; then
		TYPE=GSM_MTD
	elif [ $($BUSYBOX cat /proc/partitions | $BUSYBOX grep "mtdblock2" | $BUSYBOX grep "192000" | $BUSYBOX wc -l) == 1 ] ; then
		TYPE=GSM_MTD_CM7
	elif [ $($BUSYBOX cat /proc/partitions | $BUSYBOX grep "bml11" | $BUSYBOX grep "35840" | $BUSYBOX wc -l) == 1 ] ; then
		TYPE=GSM_BML
	elif [ $($BUSYBOX cat /proc/partitions | $BUSYBOX grep "bml11" | $BUSYBOX grep "179200" | $BUSYBOX wc -l) == 1 ] ; then
		TYPE=CDMA_BML
	fi
}


devicechecker()
{
	if [ $TYPE == "GSM_MTD" ] ; then
		exit 0
	elif
		[ $TYPE == "GSM_BML" ] ; then
		exit 1
	elif
		[ $TYPE == "GSM_MTD_CM7" ] ;then
		exit 2
	elif
		[ $TYPE == "CDMA" ] ; then
		exit 3
	fi
}

backup_efs()
{
	if [ -e /sdcard/backup/efs ] ; then
		$BUSYBOX mv /sdcard/backup/efs /sdcard/backup/efs-$$
	fi
	$BUSYBOX rm -rf /sdcard/backup/efs
	$BUSYBOX mkdir -p /sdcard/backup/efs
	$BUSYBOX cp -a /efs/* /sdcard/backup/efs
}

restore_efs()
{
	$BUSYBOX umount -l /efs
	/tmp/erase_image efs
	mount_efs
	$BUSYBOX cp -a /sdcard/backup/efs /
	$BUSYBOX chmod 0755 /efs
	$BUSYBOX umount -l /efs
}

reboot()
{
	#check if the device is MTD or not
	#since some of the other kernels we run have different set-ups we will always
	#reboot when this script is called
	if [ $($BUSYBOX mount | $BUSYBOX grep "stl" | $BUSYBOX wc -l) -gt "0" ] ; then
		#the device is not MTD
		#mount cache and clear recovery
		mount_sdcard
		exec >> /tmp/reboot_bml.log 2>&1
		mount_part cache stl11 rfs

		if ! [ -d /cache/recovery ] ; then
			$BUSYBOX mkdir /cache/recovery
		else
			$BUSYBOX rm -rf /cache/recovery/
			$BUSYBOX mkdir /cache/recovery
		fi

		#backup /efs
		mount_part efs stl3 rfs
		backup_efs

		#reboot and restart flash
		if [ $($BUSYBOX cat /tmp/recovery.log | $BUSYBOX grep "TWRP" | $BUSYBOX wc -l) -gt 0 ] ; then
			UPDATE_PACKAGE=$($BUSYBOX grep -o 'Install /sdcard/.*' /tmp/recovery.log | $BUSYBOX grep -o '/sdcard/.*' | $BUSYBOX tail -1 | $BUSYBOX sed 's| ...||g')
		fi 
		PACKAGE_LOCATION=${UPDATE_PACKAGE#/mnt}
		$BUSYBOX echo "install $PACKAGE_LOCATION" > /sdcard/openrecoveryscript

		#export tmp files to sdcard
		if [ -d /sdcard/teamnyxinstall/tmpfiles ] ; then
			$BUSYBOX cp -a /tmp/teamnyx/* /sdcard/teamnyxinstall/tmpfiles/
		else
			$BUSYBOX mkdir -p /sdcard/teamnyxinstall/tmpfiles
			$BUSYBOX cp -a /tmp/teamnyx/* /sdcard/teamnyxinstall/tmpfiles/
		fi
		if [ -d /sdcard/teamnyxinstall/tmp ] ; then
			$BUSYBOX cp /tmp/* /sdcard/teamnyxinstall/tmp/
			$BUSYBOX rm -f /sdcard/teamnyxinstall/tmp/*.log
		else
			$BUSYBOX mkdir -p /sdcard/teamnyxinstall/tmp
			$BUSYBOX cp /tmp/* /sdcard/teamnyxinstall/tmp/
			$BUSYBOX rm -f /sdcard/teamnyxinstall/tmp/*.log
		fi

		#export logs to sdcard
		if [ -d /sdcard/teamnyxlogs ] ; then
			$BUSYBOX rm -rf /sdcard/teamnyxlogs
		fi
		if ! [ -d /sdcard/teamnyxlogs/bml/ ] ; then
			$BUSYBOX mkdir -p /sdcard/teamnyxlogs/bml
			$BUSYBOX cp -a /tmp/*.log /sdcard/teamnyxlogs/bml/
		else
			$BUSYBOX rm -rf /sdcard/teamnyxlogs/bml
			$BUSYBOX mkdir -p /sdcard/teamnyxlogs/bml
			$BUSYBOX cp -a /tmp/*.log /sdcard/teamnyxlogs/bml/
		fi

		$BUSYBOX touch /sdcard/teamnyxinstall/tmpfiles/check.prop
		$BUSYBOX echo -e "continue_install=yes\n" > /sdcard/teamnyxinstall/tmpfiles/check.prop

		$BUSYBOX touch /sdcard/teamnyxinstall/continue_install

		$BUSYBOX sync
		$BUSYBOX reboot
	else
		#the device is MTD
		#mount sdcard
		mount_sdcard
		mount_part cache mtdblock3 yaffs2
		exec >> /tmp/reboot_mtd.log 2>&1

		if ! [ -d /cache/recovery ] ; then
			$BUSYBOX mkdir /cache/recovery
		else
			$BUSYBOX rm -rf /cache/recovery/extendedcommand
		fi
		
		#reboot and restart flash
		if [ $($BUSYBOX cat /tmp/recovery.log | $BUSYBOX grep "TWRP" | $BUSYBOX wc -l) -gt 0 ] ; then
			UPDATE_PACKAGE=$($BUSYBOX grep -o 'Install /sdcard/.*' /tmp/recovery.log | $BUSYBOX grep -o '/sdcard/.*' | $BUSYBOX tail -1 | $BUSYBOX sed 's| ...||g')
		fi 
		PACKAGE_LOCATION=${UPDATE_PACKAGE#/mnt}
		$BUSYBOX echo "install $PACKAGE_LOCATION" > /sdcard/openrecoveryscript

		#export tmp files to sdcard
		if [ -d /sdcard/teamnyxinstall/tmpfiles ] ; then
			$BUSYBOX cp -a /tmp/teamnyx/* /sdcard/teamnyxinstall/tmpfiles/
		else
			$BUSYBOX mkdir -p /sdcard/teamnyxinstall/tmpfiles
			$BUSYBOX cp -a /tmp/teamnyx/* /sdcard/teamnyxinstall/tmpfiles/
		fi
		if [ -d /sdcard/teamnyxinstall/tmp ] ; then
			$BUSYBOX cp /tmp/* /sdcard/teamnyxinstall/tmp/
			$BUSYBOX rm -f /sdcard/teamnyxinstall/tmp/*.log
		else
			$BUSYBOX mkdir -p /sdcard/teamnyxinstall/tmp
			$BUSYBOX cp /tmp/* /sdcard/teamnyxinstall/tmp/
			$BUSYBOX rm -f /sdcard/teamnyxinstall/tmp/*.log
		fi

		#export logs to sdcard
		if ! [ -d /sdcard/teamnyxlogs/mtd_before_reboot/ ] ; then
			$BUSYBOX mkdir -p /sdcard/teamnyxlogs/mtd_before_reboot
			$BUSYBOX cp -a /tmp/*.log /sdcard/teamnyxlogs/mtd_before_reboot/
		else
			$BUSYBOX rm -rf /sdcard/teamnyxlogs/mtd_before_reboot
			$BUSYBOX mkdir -p /sdcard/teamnyxlogs/mtd_before_reboot
			$BUSYBOX cp -a /tmp/*.log /sdcard/teamnyxlogs/mtd_before_reboot/
		fi
		
		$BUSYBOX touch /sdcard/teamnyxinstall/tmpfiles/check.prop
		$BUSYBOX echo -e "continue_install=yes\n" > /sdcard/teamnyxinstall/tmpfiles/check.prop

		$BUSYBOX touch /sdcard/teamnyxinstall/continue_install

		$BUSYBOX sync
		$BUSYBOX reboot
	fi
}

reboot_cleanup()
{
	mount_sdcard
	exec >> /tmp/reboot_cleanup.log 2>&1
	#restore /efs
	if [ -d /sdcard/backup/efs ] ; then	
		restore_efs
	fi
	#we are going to check to make sure that the efs was restored
	if [ -d /efs ] ; then
		#seems fine so far
		if [ $($BUSYBOX ls -al /efs | $BUSYBOX wc -l) -eq 2 ] ; then
			if ! [ $($BUSYBOX ls /sdcard/backup/ | $BUSYBOX awk '{if (NR == "1") print $1}') = "efs" ] ; then
				directory=$($BUSYBOX ls /sdcard/backup/ | $BUSYBOX awk '{if (NR == "1") print $1}')
				$BUSYBOX cp -a /sdcard/backup/$directory /sdcard/backup/efs
				restore_efs
			else
				if [ $($BUSYBOX ls /sdcard/backup/efs | $BUSYBOX wc -l) -gt 0 ] ; then
					directory=$($BUSYBOX ls /sdcard/backup/ | $BUSYBOX awk '{if (NR == "2") print $1}')
					$BUSYBOX cp -a /sdcard/backup/$directory /sdcard/backup/efs
					restore_efs
				else
					#well, we are just kinda screwed at this point
					$BUSYBOX echo "Could not find a backup of the efs directory"
				fi
			fi
		else
			#everything should be fine
			$BUSYBOX echo "EFS restore seems to have been completed successfully."
		fi
	else
		#something obviously didn't go right
		if ! [ $($BUSYBOX ls /sdcard/backup/ | $BUSYBOX awk '{if (NR == "1") print $1}') = "efs" ] ; then
			directory=$($BUSYBOX ls /sdcard/backup/ | $BUSYBOX awk '{if (NR == "1") print $1}')
			$BUSYBOX cp -a /sdcard/backup/$directory /sdcard/backup/efs
			restore_efs
		else
			if [ $($BUSYBOX ls /sdcard/backup/efs | $BUSYBOX wc -l) -gt 0 ] ; then
				directory=$($BUSYBOX ls /sdcard/backup/ | $BUSYBOX awk '{if (NR == "2") print $1}')
				$BUSYBOX cp -a /sdcard/backup/$directory /sdcard/backup/efs
				restore_efs
			else
				#well, we are just kinda screwed at this point
				$BUSYBOX echo "Could not find a backup of the efs directory"
			fi
		fi
	fi
	#restore tmp files
	$BUSYBOX cp -a /sdcard/teamnyxinstall/tmpfiles/* /tmp/teamnyx/
	$BUSYBOX cp /sdcard/teamnyxinstall/tmp/* /tmp/
}

modem()
{
	mount_sdcard
	exec >> /tmp/modem.log 2>&1
	#check to make sure a modem exists to flash
	if [ -e /tmp/modem.bin ] ; then
		#format /radio
		$BUSYBOX umount -l /radio
		/tmp/erase_image radio

		#make sure /radio is mounted properly
		mount_part radio mtdblock5 yaffs2

		#copy new modem.bin
		$BUSYBOX cp /tmp/modem.bin /radio/modem.bin

		#set modem.bin permissions and owner
		$BUSYBOX chmod 0600 /radio/modem.bin
		$BUSYBOX chown radio.radio /radio/modem.bin
	fi
}

checkdata()
{
	mount_sdcard
	exec >> /tmp/checkdata.log 2>&1
	#check if the device is MTD or not
	if ! [ $($BUSYBOX mount | $BUSYBOX grep "stl" | $BUSYBOX wc -l) -gt "0" ] ; then
		#the device is MTD
		#mount system
		mount_part system mtdblock2 yaffs2
		#check to see if a data wipe is going to be neceasary
		if [ $($BUSYBOX cat /system/build.prop | $BUSYBOX grep "update.icssgs.version=" | $BUSYBOX wc -l) -gt 0 ] ; then
			if [ $($BUSYBOX cat /system/build.prop | $BUSYBOX grep "update.icssgs.version=" | $BUSYBOX awk '{print $2}') -le 3 ] ; then
				exit 0
			else
				#a data wipe will not be needed, so go ahead and backup the batterystats.bin
				#unless this is a continued install
				#mount sdcard and data
				mount_sdcard
				if [ -e /sdcard/teamnyxinstall/continue_install ] ; then
					exit 1
				else
					mount_part data mmcblk0p2 ext4
					#save the batterystats.bin on the sdcard
					$BUSYBOX cp /data/system/batterystats.bin /sdcard/batterystats.bin
					exit 1
				fi
			fi
		elif [ $($BUSYBOX cat /system/build.prop | $BUSYBOX grep "update.device.version=" | $BUSYBOX wc -l) -gt 0 ] ; then
			if [ $($BUSYBOX cat /system/build.prop | $BUSYBOX grep "update.device.version=" | $BUSYBOX awk '{print $2}') -le 3 ] ; then
				exit 0
			else
				exit 1
			fi
		elif ! [ $($BUSYBOX cat /system/build.prop | $BUSYBOX grep "update.icssgs.version=" | $BUSYBOX wc -l) -gt 0 ] || [ $($BUSYBOX cat /system/build.prop | $BUSYBOX grep "update.device.version=" | $BUSYBOX wc -l) -gt 0 ] ; then
			exit 0
		fi
	else
		#the device is not MTD, so a data wipe will be needed
		#mount sdcard
		exit 0
	fi
	$BUSYBOX umount -l /sdcard/
	$BUSYBOX umount -l /system
	$BUSYBOX umount -l /data
	exit 1
}

checker()
{
	mount_sdcard
	exec >> /tmp/checker.log 2>&1

	if [ -e /sdcard/teamnyxinstall/continue_install ] ; then
		#export tmp files back to /tmp
		$BUSYBOX mkdir -p /tmp/teamnyx/
		$BUSYBOX cp -a /sdcard/teamnyxinstall/tmpfiles/* /tmp/teamnyx/
	else
		exit 1
	fi
	exit 0
}

nodatawipe()
{
	echo "selected.0=1" > /tmp/teamnyx/wipedatachoose.prop
}

mount_efs()
{
	mount_part efs mtdblock4 yaffs2
}

mount_sdcard()
{
	mount_part sdcard mmcblk0p1 vfat
}

wipedatachoose()
{
	mount_sdcard
	exec >> /tmp/wipedatachoose.log 2>&1
	if [ -e /tmp/teamnyx/wipedatachoose.prop ] ; then
		YES=$($BUSYBOX cat /tmp/teamnyx/wipedatachoose.prop | $BUSYBOX grep "selected.0=1" | $BUSYBOX wc -l)
		if [ $YES -eq 1 ] ; then
			exit 0
		else
			STATS=$($BUSYBOX cat /tmp/teamnyx/wipedata.prop | $BUSYBOX grep "wipedata=yes" | $BUSYBOX wc -l)
			if [ $STATS -eq 1 ] ; then
				exit 2
			else
				exit 1
			fi
		fi
	else
		exit 1
	fi
}

cleanup()
{
	mount_sdcard
	$BUSYBOX rm -rf /sdcard/teamnyxinstall
}

battery_stats_restore()
{
	mount_sdcard
	exec >> /tmp/battery_stats_restore.log 2>&1
	#data does not need to be wiped, so restore batterystats.bin
	#ensure that /data/system exists, just to be safe
	mount_part data mmcblk0p2 ext4
	$BUSYBOX mkdir -p /data/system/
	#restore batterystats.bin and remove the backup from the sdcard
	$BUSYBOX cp /sdcard/batterystats.bin /data/system/batterystats.bin
	$BUSYBOX rm -rf /sdcard/batterystats.bin
}

save_logs()
{
	mount_sdcard
	if [ -e /sdcard/install_log ] ; then
		$BUSYBOX cp /sdcard/install_log /sdcard/teamnyxlogs/mtd_after_reboot/install.log
		$BUSYBOX rm -f /sdcard/install_log
	else
		$BUSYBOX rm -rf /sdcard/teamnyxlogs
		$BUSYBOX rm -f /sdcard/install_log
	fi
}

dataapp()
{
	mount_part data mmcblk0p2 ext4
	$BUSYBOX mkdir -p /data/app
}

kernel_check()
{
	#export logs
	exec >> /tmp/kernel_check.log 2>&1
	#find out the location of the .zip
	#find out how many kernels there are available to install
	K=$($BUSYBOX cat /tmp/teamnyx/kernelinfo | $BUSYBOX wc -l)
	K=$($BUSYBOX expr $K - 1)
	$BUSYBOX echo "There are $K kernel(s) to choose from."
	#set the number of kernels counter
	NK=0
	#set the language
	lang=$($BUSYBOX cat /tmp/teamnyx/language.prop | $BUSYBOX grep "yes" | $BUSYBOX wc -l)
	if [ $lang -gt 0 ] ; then
		lang=english
	else
		lang=spanish
	fi
	#make the list of the kernels in .prop form
	while [ $NK -lt $K ]
	do
		#add one to NK, to increase it after every run
		NK=$($BUSYBOX expr $NK + 1)
		#set variables for each element of the kernelinfo file
		dir_name=$($BUSYBOX cat /tmp/teamnyx/kernelinfo | $BUSYBOX awk -v nk="$NK" '$1 == nk {print $2}')
		en_title=$($BUSYBOX cat /tmp/teamnyx/kernelinfo | $BUSYBOX awk -v nk="$NK" '$1 == nk {print $3}')
		en_subtitle=$($BUSYBOX cat /tmp/teamnyx/kernelinfo | $BUSYBOX awk -v nk="$NK" '$1 == nk {print $4}')
		sp_title=$($BUSYBOX cat /tmp/teamnyx/kernelinfo | $BUSYBOX awk -v nk="$NK" '$1 == nk {print $5}')
		sp_subtitle=$($BUSYBOX cat /tmp/teamnyx/kernelinfo | $BUSYBOX awk -v nk="$NK" '$1 == nk {print $6}')
		#write the .prop file
		if [ $lang = "english" ] ; then
			en_title=$($BUSYBOX echo $en_title | $BUSYBOX sed 's|_| |g')
			en_subtitle=$($BUSYBOX echo $en_subtitle | $BUSYBOX sed 's|_| |g')
			$BUSYBOX echo "kernel$NK.arg1=$en_title" >> /tmp/teamnyx/kernel_choose.prop
			$BUSYBOX echo "kernel$NK.arg2=$en_subtitle"  >> /tmp/teamnyx/kernel_choose.prop
			$BUSYBOX echo "kernel$NK.location=$dir_name" >> /tmp/teamnyx/kernel_choose.prop
		else
			sp_title=$($BUSYBOX echo $sp_title | $BUSYBOX sed 's|_| |g')
			sp_subtitle=$($BUSYBOX echo $sp_subtitle | $BUSYBOX sed 's|_| |g')
			$BUSYBOX echo "kernel$NK.arg1=$sp_title" >> /tmp/teamnyx/kernel_choose.prop
			$BUSYBOX echo "kernel$NK.arg2=$sp_subtitle"  >> /tmp/teamnyx/kernel_choose.prop
			$BUSYBOX echo "kernel$NK.location=$dir_name" >> /tmp/teamnyx/kernel_choose.prop
		fi
	done
}

apps_check()
{
	#export logs
	exec >> /tmp/apps_check.log 2>&1
	#find out how many apps there are available to install
	A=$($BUSYBOX cat /tmp/teamnyx/appsinfo | $BUSYBOX wc -l)
	A=$($BUSYBOX expr $A - 2)
	$BUSYBOX echo "There are $A app(s) to choose from."
	#set the app number and group number variables
	NA=0
	#determine the number of groups
	NG=$($BUSYBOX expr $($BSUYBOX cat /tmp/teamnyx/appsinfo | $BUSYBOX awk '{if ($1 == "group") print $0}' | $BUSYBOX wc -w) - 1)
	#make sure there is a clean /tmp/groups
	if [ -e /tmp/groups ] ; then
		$BUSYBOX rm -f /tmp/groups
		$BUSYBOX touch /tmp/groups
	else
		$BUSYBOX touch /tmp/groups
	fi
	SB=0
	#make a list of the groups in /tmp/groups
	while [ $SB -lt $NG ]
	do
		SB=$($BUSYBOX expr $SB + 1)
		ZA=$($BUSYBOX expr $SB + 1)
		ZA=$($BUSYBOX cat /tmp/teamnyx/appsinfo | $BUSYBOX awk -v e="$ZA" '$1 == "group" {print $e}')
		$BUSYBOX echo "$SB $ZA" >> /tmp/groups
	done
	#verify the number of groups just to be certain
	NG=$($BUSYBOX cat /tmp/groups | $BUSYBOX wc -l)
	#make sure there is a clean /tmp/apps
	if [ -e /tmp/apps ] ; then
		$BUSYBOX rm -f /tmp/apps
		$BUSYBOX touch /tmp/apps
	else
		$BUSYBOX touch /tmp/apps
	fi
	#write the apps and groups to the /tmp/group# files in the correct order
	SA=0
	while [ $SA -lt $NG ]
	do
		if [ -e /tmp/info ] ; then
			$BUSYBOX rm -f /tmp/info
			$BUSYBOX touch /tmp/info
		else
			$BUSYBOX touch /tmp/info
		fi
		SA=$($BUSYBOX expr $SA + 1)
		GN=$($BUSYBOX cat /tmp/groups | $BUSYBOX awk -v sa="$SA" '{if ($1 == sa) print $2}')
		$BUSYBOX echo "$GN" >> /tmp/group$SA
		NAG=$($BUSYBOX expr $($BUSBYOX cat /tmp/teamnyx/appsinfo | $BUSYBOX grep "$GN" | $BUSYBOX wc -l) - 1)
		$BUSYBOX echo $($BUSYBOX cat /tmp/teamnyx/appsinfo | $BUSYBOX awk -v gn="$GN" '{if ($8 == gn) print $1}') >> /tmp/info
		RNG=0
		if [ -e /tmp/tempfile ] ; then
			$BUSYBOX rm -f /tmp/tempfile
			$BUSYBOX touch /tmp/tempfile
		else
			$BUSYBOX touch /tmp/tempfile
		fi
		while [ $RNG -lt $NAG ]
		do
			RNG=$($BUSYBOX expr $RNG + 1)
			$BUSYBOX echo $($BUSYBOX cat /tmp/info | $BUSYBOX awk -v rng="$RNG" '{print $rng}') >> /tmp/tempfile
		done
		$BUSYBOX tail /tmp/tempfile >> /tmp/group$SA
	done
	lang=$($BUSYBOX cat /tmp/teamnyx/language.prop | $BUSYBOX grep "yes" | $BUSYBOX wc -l)
	if [ $lang -gt 0 ] ; then
		lang=english
	else
		lang=spanish
	fi
	SA=0
	ANX=0
	if [ $lang = "english" ] ; then
		while [ $SA -lt $NG ]
		do
			SA=$($BUSYBOX expr $SA + 1)
			ANX=$($BUSYBOX expr $ANX + 1)
			#write group name to .prop
			group_name=$($BUSYBOX cat /tmp/group$SA | $BUSYBOX awk '{if (NR == "1") print $1}' | $BUSYBOX sed 's/-=-.*//g')
			$BUSYBOX echo "app$ANX.arg1=$group_name" >> /tmp/teamnyx/apps_choose.prop
			$BUSYBOX echo "app$ANX.arg2=" >> /tmp/teamnyx/apps_choose.prop
			$BUSYBOX echo "app$ANX.arg3=2" >> /tmp/teamnyx/apps_choose.prop
			#find out the number of apps in the group
			NOAG=$($BUSYBOX cat /tmp/group$SA | $BUSYBOX wc -l) ; NOAG=$($BUSYBOX expr $NOAG - 1)
			NAWO=0
			while [ $NAWO -lt $NOAG ]
			do
				ANX=$($BUSYBOX expr $ANX + 1)
				NAWO=$($BUSYBOX expr $NAWO + 1)
				#the app number will be what is written in the next line of /tmp/group$SA
				LNAWO=$($BUSYBOX expr $NAWO + 1)
				ALN=$($BUSYBOX cat /tmp/group$SA | $BUSYBOX awk -v stuff="$LNAWO" '{if (NR == stuff) print $1}')
				title=$($BUSYBOX cat /tmp/teamnyx/appsinfo | $BUSYBOX awk -v aln="$ALN" '$1 == aln {print $3}' | $BUSYBOX sed 's|_| |g')
				subtitle=$($BUSYBOX cat /tmp/teamnyx/appsinfo | $BUSYBOX awk -v aln="$ALN" '$1 == aln {print $4}' | $BUSYBOX sed 's|_| |g')
				if [ $($BUSYBOX cat /tmp/teamnyx/appsinfo | $BUSYBOX awk -v aln="$ALN" '$1 == aln {print $7}') = "recommended" ] ; then
					type=1
				else
					type=0
				fi
				#write the .prop file
				$BUSYBOX echo "app$ANX.arg1=$title" >> /tmp/teamnyx/apps_choose.prop
				$BUSYBOX echo "app$ANX.arg2=$subtitle"  >> /tmp/teamnyx/apps_choose.prop
				$BUSYBOX echo "app$ANX.arg3=$type" >> /tmp/teamnyx/apps_choose.prop
			done
		done
	else
		while [ $SA -lt $NG ]
		do
			SA=$($BUSYBOX expr $SA + 1)
			ANX=$($BUSYBOX expr $ANX + 1)
			#write group name to .prop
			group_name=$($BUSYBOX cat /tmp/group$SA | $BUSYBOX awk '{if (NR == "1") print $1}' | $BUSYBOX sed 's/.*-=-//g')
			$BUSYBOX echo "app$ANX.arg1=$group_name" >> /tmp/teamnyx/apps_choose.prop
			$BUSYBOX echo "app$ANX.arg2=" >> /tmp/teamnyx/apps_choose.prop
			$BUSYBOX echo "app$ANX.arg3=2" >> /tmp/teamnyx/apps_choose.prop
			#find out the number of apps in the group
			NOAG=$($BUSYBOX cat /tmp/group$SA | $BUSYBOX wc -l) ; NOAG=$($BUSYBOX expr $NOAG - 1)
			NAWO=0
			while [ $NAWO -lt $NOAG ]
			do
				ANX=$($BUSYBOX expr $ANX + 1)
				NAWO=$($BUSYBOX expr $NAWO + 1)
				#the app number will be what is written in the next line of /tmp/group$SA
				LNAWO=$($BUSYBOX expr $NAWO + 1)
				ALN=$($BUSYBOX cat /tmp/group$SA | $BUSYBOX awk -v stuff="$LNAWO" '{if (NR == stuff) print $1}')
				title=$($BUSYBOX cat /tmp/teamnyx/appsinfo | $BUSYBOX awk -v aln="$ALN" '$1 == aln {print $5}' | $BUSYBOX sed 's|_| |g')
				subtitle=$($BUSYBOX cat /tmp/teamnyx/appsinfo | $BUSYBOX awk -v aln="$ALN" '$1 == aln {print $6}' | $BUSYBOX sed 's|_| |g')
				if [ $($BUSYBOX cat /tmp/teamnyx/appsinfo | $BUSYBOX awk -v aln="$ALN" '$1 == aln {print $7}') = "recommended" ] ; then
					type=1
				else
					type=0
				fi
				#write the .prop file
				$BUSYBOX echo "app$ANX.arg1=$title" >> /tmp/teamnyx/apps_choose.prop
				$BUSYBOX echo "app$ANX.arg2=$subtitle"  >> /tmp/teamnyx/apps_choose.prop
				$BUSYBOX echo "app$ANX.arg3=$type" >> /tmp/teamnyx/apps_choose.prop
			done
		done
	fi
}

install_addons()
{
	exec >> /tmp/install_addons.log 2>&1
	NG=$($BUSYBOX cat /tmp/groups | $BUSYBOX wc -l)
	GWO=0
	mount_part cache mtdblock3 yaffs2
	PACKAGE_LOCATION=$($BUSYBOX cat /sdcard/openrecoveryscript | $BUSYBOX sed 's|install ||g')
	while [ $GWO -lt $NG ]
	do
		GWO=$($BUSYBOX expr $GWO + 1)
		NA=$($BUSYBOX expr $($BUSYBOX cat /tmp/group$GWO | $BUSYBOX wc -l) - 1)
		AWO=0
		while [ $AWO -lt $NA ]
		do
			AWO=$($BUSYBOX expr $AWO + 1)
			install=$($BUSYBOX cat /tmp/teamnyx/applications.prop | $BUSYBOX grep "item.$GWO.$AWO" | $BUSYBOX sed 's|.*=||g')
			if [ $install -eq "1" ] ; then
				AN=$($BUSYBOX expr $AWO + 1)
				AN=$($BUSYBOX cat /tmp/group$GWO | $BUSYBOX awk -v an="$AN" '{if (NR == an) print $1}')
				type=$($BUSYBOX cat /tmp/teamnyx/appsinfo | $BUSYBOX awk -v an="$AN" '{if ($1 == an) print $9}')
				if [ $type = "data_app" ] ; then
					name=$($BUSYBOX cat /tmp/teamnyx/appsinfo | $BUSYBOX awk -v an="$AN" '{if ($1 == an) print $2}')
					mount_part data mmcblk0p2 ext4
					$BUSYBOX unzip $PACKAGE_LOCATION "applications/$name" -d /tmp
					$BUSYBOX cp -f /tmp/applications/$name /data/app/$name
				elif [ $type = "system_app" ] ; then
					name=$($BUSYBOX cat /tmp/teamnyx/appsinfo | $BUSYBOX awk -v an="$AN" '{if ($1 == an) print $2}')
					if ! [$($BUSYBOX cat /proc/mounts | $BUSYBOX grep "system" | $BUSYBOX grep "rw" | $BUSYBOX wc -l) -eq 1 ] ; then
						$BUSYBOX mount -o rw,remount -t yaffs2 /dev/block/mtdblock2 /system
					fi
					$BUSYBOX unzip $PACKAGE_LOCATION "applications/$name" -d /tmp
					$BUSYBOX cp -f /tmp/applications/$name /systems/app/$name
				elif [ $type = "bootanimation" ] ; then
					name=$($BUSYBOX cat /tmp/teamnyx/appsinfo | $BUSYBOX awk -v an="$AN" '{if ($1 == an) print $2}')
					$BUSYBOX unzip $PACKAGE_LOCATION "applications/$name" -d /tmp
					if ! [$($BUSYBOX cat /proc/mounts | $BUSYBOX grep "system" | $BUSYBOX grep "rw" | $BUSYBOX wc -l) -eq 1 ] ; then
						$BUSYBOX mount -o rw,remount -t yaffs2 /dev/block/mtdblock2 /system
					fi
					$BUSYBOX cp -f /tmp/applications/$name /system/media/bootanimation.zip
				else
					$BUSYBOX unzip $PACKAGE_LOCATION "applications/$type" -d /tmp
					$BUSYBOX sh /tmp/$type
				fi
			fi
		done
	done
	$BUSYBOX rm -rf /tmp/applications
}

write_tmp()
{
	exec >> /tmp/write_tmp.log 2>&1
	NG=$($BUSYBOX cat /tmp/groups | $BUSYBOX wc -l)
	GWO=0
	if [ -e /tmp/teamnyx/applications.prop ] ; then
		$BUSYBOX rm -f /tmp/teamnyx/applications.prop
		$BUSYBOX touch /tmp/teamnyx/applications.prop
	else
		$BUSYBOX touch /tmp/teamnyx/applications.prop
	fi
	while [ $GWO -lt $NG ]
	do
		GWO=$($BUSYBOX expr $GWO + 1)
		NA=$($BUSYBOX expr $($BUSYBOX cat /tmp/group$GWO | $BUSYBOX wc -l) - 1)
		AWO=0
		while [ $AWO -lt $NA ]
		do
			AWO=$($BUSYBOX expr $AWO + 1)
			AN=$($BUSYBOX expr $AWO + 1)
			AN=$($BUSYBOX cat /tmp/group$GWO | $BUSYBOX awk -v an="$AN" '{if (NR == an) print $1}')
			type=$($BUSYBOX cat /tmp/teamnyx/appsinfo | $BUSYBOX awk -v an="$AN" '{if ($1 == an) print $7}')
			if [ $type = "recommended" ] ; then
				type=1
			else
				type=0
			fi
			$BUSYBOX echo "item.$GWO.$AWO=$type" >> /tmp/teamnyx/applications.prop
		done
	done
}

write_tmp_aosp()
{
	exec >> /tmp/write_tmp_aosp.log 2>&1
	NG=$($BUSYBOX cat /tmp/groups | $BUSYBOX wc -l)
	GWO=0
	if [ -e /tmp/teamnyx/applications.prop ] ; then
		$BUSYBOX rm -f /tmp/teamnyx/applications.prop
		$BUSYBOX touch /tmp/teamnyx/applications.prop
	else
		$BUSYBOX touch /tmp/teamnyx/applications.prop
	fi
	while [ $GWO -lt $NG ]
	do
		GWO=$($BUSYBOX expr $GWO + 1)
		NA=$($BUSYBOX expr $($BUSYBOX cat /tmp/group$GWO | $BUSYBOX wc -l) - 1)
		AWO=0
		while [ $AWO -lt $NA ]
		do
			AWO=$($BUSYBOX expr $AWO + 1)
			$BUSYBOX echo "item.$GWO.$AWO=0" >> /tmp/teamnyx/applications.prop
		done
	done
}

export_logs()
{
	mount_sdcard
	if [ -d /sdcard/teamnyxlogs/mtd_after_reboot ] ; then
		$BUSYBOX rm -rf /sdcard/teamnyxlogs/mtd_after_reboot
		$BUSYBOX mkdir -p /sdcard/teamnyxlogs/mtd_after_reboot
		$BUSYBOX cp -a /tmp/*.log /sdcard/teamnyxlogs/mtd_after_reboot/
	else
		$BUSYBOX mkdir -p /sdcard/teamnyxlogs/mtd_after_reboot
		$BUSYBOX cp -a /tmp/*.log /sdcard/teamnyxlogs/mtd_after_reboot/
	fi
}

delete_cfg()
{
	mount_sdcard
	$BUSYBOX rm -f /sdcard/cyanogenmod.cfg
	$BUSYBOX rm -f /cache/openrecoveryscript
	$BUSYBOX rm -f /sdcard/openrecoveryscript
	$BUSYBOX rm -f /cache/recovery/openrecoveryscript
}

twrp()
{
	if [ $($BUSYBOX cat "/tmp/teamnyx/kernel.prop" | $BUSYBOX grep "selected.0=1" | $BUSYBOX wc -l) -eq 1 ] ; then
		mount_part cache mtdblock3 yaffs2
		$BUSYBOX touch /cache/twrp
	fi
}

clean_previous_install()
{
	mount_sdcard
	if ! [ -e /sdcard/teamnyxinstall/continue_install ] ; then
		$BUSYBOX rm -rf /sdcard/teamnyxinstall
		$BUSYBOX rm -rf /sdcard/teamnyxlogs
		$BUSYBOX rm /tmp/teamnyx/appsinfo
		$BUSYBOX rm /tmp/teamnyx/kernelinfo
		$BUSYBOX rm /tmp/teamnyx/apps_choose.prop
		$BUSYBOX rm /tmp/teamnyx/kernel_choose.prop
	fi
}

change_recovery()
{
	$BUSYBOX tar -C / -zxvf /tmp/rec.tgz
	pid=$($BUSYBOX pidof recovery)
	$BUSYBOX kill $pid
}


$1
$2
