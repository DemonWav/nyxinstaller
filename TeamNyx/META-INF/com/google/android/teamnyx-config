# Installer Config for Team Nyx
# © 2012 Team Nyx - DemonWav
# Written by DemonWav (Kyle Wood)

# This script is part of the Team Nyx AROMA Installer, all credits
# for the AROMA binary, as well as the source code and documentation
# of the binary go to amarullz. Big thanks to him for the great help
# he was in this whole process.

# This file is part of the Team Nyx AROPMA Installer.
# The installer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# The installer is distributed in hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details. See
# <http://www.gnu.org/licenses/> for more information.

restotmp("busybox", "busybox");
if
	resexec("teamnyx.sh", "partitionchecker", "devicechecker") == "0"
then
	writetmpfile(
		"calibrate.prop",
		"device=GSM_MTD"
	);
endif;
if
	resexec("teamnyx.sh", "partitionchecker", "devicechecker") == "1"
then
	writetmpfile(
		"calibrate.prop",
		"device=GSM_BML"
	);
endif;
if
	resexec("teamnyx.sh", "partitionchecker", "devicechecker") == "2"
then
	writetmpfile(
		"calibrate.prop",
		"device=GSM_MTD_CM7"
	);
endif;
if
	resexec("teamnyx.sh", "partitionchecker", "devicechecker") == "3"
then
	writetmpfile(
		"calibrate.prop",
		"device=CDMA"
	);
endif;

if
	file_getprop("/tmp/teamnyx-data/calibrate.prop", "device")=="GSM_MTD" || file_getprop("/tmp/teamnyx-data/calibrate.prop", "device")=="GSM_MTD_CM7"
then
	#GSM_MTD
	calibrate("1.8447", "37", "1.2158", "27");
	ini_set("customkeycode_up",     "115");
	ini_set("customkeycode_down",   "114");
	ini_set("customkeycode_select", "116");
	ini_set("customkeycode_menu",   "139");
	ini_set("customkeycode_back",   "158");
endif;
if
	file_getprop("/tmp/teamnyx-data/calibrate.prop", "device")=="CDMA"
then
	#CDMA
	calibrate("0.9561", "11", "0.9682", "20", "yes");
	ini_set("customkeycode_up",     "51");
	ini_set("customkeycode_down",   "52");
	ini_set("customkeycode_select", "116");
	ini_set("customkeycode_menu",   "139");
	ini_set("customkeycode_back",   "158");
endif;
if
	file_getprop("/tmp/teamnyx-data/calibrate.prop", "device")=="GSM_BML"
then
	#GSM_BML
	calibrate("0.8824", "41", "0.9469", "24", "yes");
	ini_set("customkeycode_up",     "115");
	ini_set("customkeycode_down",   "114");
	ini_set("customkeycode_select", "116");
	ini_set("customkeycode_menu",   "139");
	ini_set("customkeycode_back",   "158");
endif;

#########################

#Initializing Rom Information
ini_set("rom_name",             "AOSP ICS");
ini_set("rom_version",          "RC5");
ini_set("rom_author",           "Team Nyx");
ini_set("rom_device",           "Samsung Galaxy S");

#Show splash screen for 2 seconds
splash(2000, "splash");
theme("ics");

ini_set("force_colorspace","rgba");

fontresload("0","DroidSans.ttf","12");
fontresload("1","DroidSans.ttf","18");

pleasewait("Preparing installation...");

#########################
#########################
#########################

resexec("teamnyx.sh", "checker");
resexec("teamnyx.sh", "clean_previous_install");
if
	file_getprop("/tmp/teamnyx-data/check.prop", "continue_install")=="yes"
then
	if
		prop("language.prop","english")=="yes"
	then
		setvar("lang","en.prop");
	else
		setvar("lang","sp.prop");
	endif;

	ini_set("text_next", resprop(getvar("lang"),"35a"));
	ini_set("text_back", resprop(getvar("lang"),"35b"));
	ini_set("powered_text", resprop(getvar("lang"),"INI1"));
	ini_set("thanks_text", resprop(getvar("lang"),"INI2"));
	ini_set("copyright_text", resprop(getvar("lang"),"INI3"));
	ini_set("installer_text", resprop(getvar("lang"),"INI4"));
	ini_set("save_logs_message1", resprop(getvar("lang"),"INI5a"));
	ini_set("save_logs_message2", resprop(getvar("lang"),"INI5b"));
	ini_set("save_logs", resprop(getvar("lang"),"INI6"));
	ini_set("save_logs_title", resprop(getvar("lang"),"INI7"));
	ini_set("save_logs_confirm", resprop(getvar("lang"),"INI8"));
	ini_set("text_about", resprop(getvar("lang"),"INI9"));
	ini_set("text_calibrating", resprop(getvar("lang"),"INI10"));
	ini_set("text_quit", resprop(getvar("lang"),"INI11"));
	ini_set("text_quit_msg", resprop(getvar("lang"),"INI12"));
	ini_set("text_yes", resprop(getvar("lang"),"17a"));

	resexec("teamnyx.sh", "mount_sdcard");
	install(
		resprop(getvar("lang"),"36"),
		resprop(getvar("lang"),"37a")+"\n"+
		resprop(getvar("lang"),"37b"),
		"@install"
	);

	#if they chose to save logs then export all of the logs to /sdcard/teamnyxlogs
	restotmp("busybox", "busybox");
	resexec("teamnyx.sh", "save_logs");
	
	#reboot dialogue
	if
		confirm(
			resprop(getvar("lang"),"38"),
			resprop(getvar("lang"),"39"),
			"@info",
			resprop(getvar("lang"),"40"),
			resprop(getvar("lang"),"41")
		)=="yes"
	then
		reboot("onfinish");
	endif;
	
else
	#########################
	#Determine if a data wipe will be needed later
	if
		resexec("teamnyx.sh", "checkdata") == "0"
	then
		writetmpfile(
			"wipedata.prop",
			"wipedata=yes"
		);
	endif;

	#########################
	#Select language
	menubox(
		"Language",
		"Please select your language.",
		"@language",
		"selectlang.prop",
		"English",    "The installation will be in English.",   "@english",
		"Español",    "El proceso de instalación continuara en español.",   "@spanish"
	);

	if
		prop("selectlang.prop","selected")=="1"
	then
		setvar("lang","en.prop");
		writetmpfile(
			"language.prop",
			"english=yes"
		);
	else
		setvar("lang","sp.prop");
		writetmpfile(
			"language.prop",
			"english=no"
		);
	endif;

	ini_set("text_yes", resprop(getvar("lang"),"INI13"));
	ini_set("text_next", resprop(getvar("lang"),"35a"));
	ini_set("text_back", resprop(getvar("lang"),"35b"));
	ini_set("powered_text", resprop(getvar("lang"),"INI1"));
	ini_set("thanks_text", resprop(getvar("lang"),"INI2"));
	ini_set("copyright_text", resprop(getvar("lang"),"INI3"));
	ini_set("installer_text", resprop(getvar("lang"),"INI4"));
	ini_set("save_logs_message1", resprop(getvar("lang"),"INI5a"));
	ini_set("save_logs_message2", resprop(getvar("lang"),"INI5b"));
	ini_set("save_logs", resprop(getvar("lang"),"INI6"));
	ini_set("save_logs_title", resprop(getvar("lang"),"INI7"));
	ini_set("save_logs_confirm", resprop(getvar("lang"),"INI8"));
	ini_set("text_about", resprop(getvar("lang"),"INI9"));
	ini_set("text_calibrating", resprop(getvar("lang"),"INI10"));
	ini_set("text_quit", resprop(getvar("lang"),"INI11"));
	ini_set("text_quit_msg", resprop(getvar("lang"),"INI12"));
	ini_set("text_yes", resprop(getvar("lang"),"17a"));

	#########################
	#Determine the kernels & apps that the user will be able to choose from
	restotmp("apps_choose.prop", "apps_choose.prop");
	restotmp("appsinfo", "appsinfo");
	restotmp("kernel_choose.prop", "kernel_choose.prop");
	restotmp("kernelinfo", "kernelinfo");
	
	resexec("teamnyx.sh", "kernel_check");
	resexec("teamnyx.sh", "apps_check");

	#########################
	#Welcome screen
	viewbox(
		resprop(getvar("lang"),"1"),
		resprop(getvar("lang"),"2a")+"\n\n"+
		resprop(getvar("lang"),"2b")+"\n"+
		resprop(getvar("lang"),"2c")+"\n"+
		resprop(getvar("lang"),"2d")+"\n\n\n"+
		resprop(getvar("lang"),"2e"),
		"@info"
	);

	#########################
	#Terms of use agreement
	agreebox(
		resprop(getvar("lang"),"3"),
		resprop(getvar("lang"),"4"),
		"@agreement",
		resread(resprop(getvar("lang"),"5")),
		resprop(getvar("lang"),"6"),
		resprop(getvar("lang"),"7")
	);

	#########################
	#Choose which install type
	menubox(
		resprop(getvar("lang"),"8"),
		resprop(getvar("lang"),"9"),
		"@install",
		"type.prop",
		resprop(getvar("lang"),"10"), resprop(getvar("lang"),"11"), "@install",
		resprop(getvar("lang"),"12"), resprop(getvar("lang"),"13"), "@apps",
		resprop(getvar("lang"),"14"), resprop(getvar("lang"),"15"), "@apps"
	);

	#########################
	#     AOSP INSTALL      #
	#########################
	if
		file_getprop("/tmp/teamnyx-data/type.prop","selected")=="1"
		# this will be an AOSP install
	then
		#######################
		# choose which kernel and build.prop to install
		menubox(
			resprop(getvar("lang"),"16"),
			resprop(getvar("lang"),"17"),
			"@apps",
			"phone.prop",
			"Galaxy S","I9000", "@install",
			"Galaxy S B","I9000B", "@install",
			"Captivate","I897/I896", "@install",
			"Vibrant","T959", "@install"
		);

		#######################
		#Confirm they have chosen the correct phone
		#Galaxy S
		if
			file_getprop("/tmp/teamnyx-data/phone.prop","selected")=="1"
		then
			if
				confirm(
					"Galaxy S",
					resprop(getvar("lang"),"18"),
					"@info",
					resprop(getvar("lang"),"17a"),
					"No"
				)=="no"
			then
				back("");
			endif;
		endif;

		#Galaxy S B
		if
			file_getprop("/tmp/teamnyx-data/phone.prop","selected")=="2"
		then
			if
				confirm(
					"Galaxy S B",
					resprop(getvar("lang"),"19"),
					"@info",
					resprop(getvar("lang"),"17a"),
					"No"
				)=="no"
			then
				back("");
			endif;
		endif;

		#Captivate
		if
			file_getprop("/tmp/teamnyx-data/phone.prop","selected")=="3"
		then
			if
				confirm(
					"Captivate",
					resprop(getvar("lang"),"20"),
					"@info",
					resprop(getvar("lang"),"17a"),
					"No"
				)=="no"
			then
				back("");
			endif;
		endif;

		#Vibrant
		if
			file_getprop("/tmp/teamnyx-data/phone.prop","selected")=="4"
		then
			if
				confirm(
					"Vibrant",
					resprop(getvar("lang"),"21"),
					"@info",
					resprop(getvar("lang"),"17a"),
					"No"
				)=="no"
			then
				back("");
			endif;
		endif;
		#########################
		#       WIPE DATA       #
		#########################
		if
			file_getprop("/tmp/teamnyx-data/wipedata.prop","wipedata")=="yes"
		then
			selectbox(
				resprop(getvar("lang"),"22"),
				resprop(getvar("lang"),"23"),
				"@apps",
				"wipedatachoose.prop",
				resprop(getvar("lang"),"24"), resprop(getvar("lang"),"25"),1,
				resprop(getvar("lang"),"26"), resprop(getvar("lang"),"27"),0
			);
			#######################		CHOSE NOT TO WIPE
			#######################
			if
				file_getprop("/tmp/teamnyx-data/wipedatachoose.prop","selected.0")=="2"
			then
				if
					confirm(
						resprop(getvar("lang"),"28"),
						resprop(getvar("lang"),"29a")+"\n"+
						resprop(getvar("lang"),"29b"),
						"@info",
						resprop(getvar("lang"),"30"),
						resprop(getvar("lang"),"31")
					)=="yes"
				then
					resexec("teamnyx.sh", "nodatawipe");
					back("");
				endif;
			endif;
		endif;
		#########################
		#     END DATA WIPE     #
		#########################
		resexec("teamnyx.sh", "write_tmp_aosp");

		writetmpfile(
		"kernel.prop",
		"selected.0=1"
		);

		ini_set("text_next", resprop(getvar("lang"),"34"));

		#Galaxy S Install
		if
			file_getprop("/tmp/teamnyx-data/phone.prop", "selected")=="1"
		then
			viewbox(
			resprop(getvar("lang"),"32"),

			resprop(getvar("lang"),"33a")+"\n\n"+
			resprop(getvar("lang"),"33b1")+"\n\n"+
			resprop(getvar("lang"),"33c"),

			"@install"
			);

			ini_set("text_next", resprop(getvar("lang"),"35a"));

			resexec("teamnyx.sh", "mount_sdcard");
			install(
				resprop(getvar("lang"),"36"),
				resprop(getvar("lang"),"37a1")+"\n"+
				resprop(getvar("lang"),"37b"),
				"@install"
			);
		endif;
		#Galaxy S B Install
		if
			file_getprop("/tmp/teamnyx-data/phone.prop", "selected")=="2"
		then
			viewbox(
			resprop(getvar("lang"),"32"),

			resprop(getvar("lang"),"33a")+"\n\n"+
			resprop(getvar("lang"),"33b2")+"\n\n"+
			resprop(getvar("lang"),"33c"),

			"@install"
			);

			ini_set("text_next", resprop(getvar("lang"),"35a"));

			resexec("teamnyx.sh", "mount_sdcard");
			install(
				resprop(getvar("lang"),"36"),
				resprop(getvar("lang"),"37a2")+"\n"+
				resprop(getvar("lang"),"37b"),
				"@install"
			);
		endif;
		#Captivate Install
		if
			file_getprop("/tmp/teamnyx-data/phone.prop", "selected")=="3"
		then
			viewbox(
			resprop(getvar("lang"),"32"),

			resprop(getvar("lang"),"33a")+"\n\n"+
			resprop(getvar("lang"),"33b3")+"\n\n"+
			resprop(getvar("lang"),"33c"),

			"@install"
			);

			ini_set("text_next", resprop(getvar("lang"),"35a"));

			resexec("teamnyx.sh", "mount_sdcard");
			install(
				resprop(getvar("lang"),"36"),
				resprop(getvar("lang"),"37a3")+"\n"+
				resprop(getvar("lang"),"37b"),
				"@install"
			);
		endif;
		#Vibrant Install
		if
			file_getprop("/tmp/teamnyx-data/phone.prop", "selected")=="4"
		then
			viewbox(
			resprop(getvar("lang"),"32"),

			resprop(getvar("lang"),"33a")+"\n\n"+
			resprop(getvar("lang"),"33b4")+"\n\n"+
			resprop(getvar("lang"),"33c"),

			"@install"
			);

			ini_set("text_next", resprop(getvar("lang"),"35a"));

			resexec("teamnyx.sh", "mount_sdcard");
			install(
				resprop(getvar("lang"),"36"),
				resprop(getvar("lang"),"37a4")+"\n"+
				resprop(getvar("lang"),"37b"),
				"@install"
			);
		endif;
	endif;
	#########################
	#  RECOMMENDED INSTALL  #
	#########################
	if
		file_getprop("/tmp/teamnyx-data/type.prop","selected")=="2"
		#this will be a recommended install
	then
		#########################
		#choose which kernel to install
		menubox(
			resprop(getvar("lang"),"16"),
			resprop(getvar("lang"),"17"),
			"@apps",
			"phone.prop",
			"Galaxy S","I9000", "@install",
			"Galaxy S B","I9000B", "@install",
			"Captivate","I897/I896", "@install",
			"Vibrant","T959", "@install"
		);

		#######################
		#Confirm they have chosen the correct phone
		#Galaxy S
		if
			file_getprop("/tmp/teamnyx-data/phone.prop","selected")=="1"
		then
			if
				confirm(
					"Galaxy S",
					resprop(getvar("lang"),"18"),
					"@info",
					resprop(getvar("lang"),"17a"),
					"No"
				)=="no"
			then
				back("");
			endif;
		endif;

		#Galaxy S B
		if
			file_getprop("/tmp/teamnyx-data/phone.prop","selected")=="2"
		then
			if
				confirm(
					"Galaxy S B",
					resprop(getvar("lang"),"19"),
					"@info",
					resprop(getvar("lang"),"17a"),
					"No"
				)=="no"
			then
				back("");
			endif;
		endif;

		#Captivate
		if
			file_getprop("/tmp/teamnyx-data/phone.prop","selected")=="3"
		then
			if
				confirm(
					"Captivate",
					resprop(getvar("lang"),"20"),
					"@info",
					resprop(getvar("lang"),"17a"),
					"No"
				)=="no"
			then
				back("");
			endif;
		endif;

		#Vibrant
		if
			file_getprop("/tmp/teamnyx-data/phone.prop","selected")=="4"
		then
			if
				confirm(
					"Vibrant",
					resprop(getvar("lang"),"21"),
					"@info",
					resprop(getvar("lang"),"17a"),
					"No"
				)=="no"
			then
				back("");
			endif;
		endif;
		#########################
		#       WIPE DATA       #
		#########################
		if
			file_getprop("/tmp/teamnyx-data/wipedata.prop","wipedata")=="yes"
		then
			selectbox(
				resprop(getvar("lang"),"22"),
				resprop(getvar("lang"),"23"),
				"@apps",
				"wipedatachoose.prop",
				resprop(getvar("lang"),"24"), resprop(getvar("lang"),"25"),1,
				resprop(getvar("lang"),"26"), resprop(getvar("lang"),"27"),0
			);
			#######################		CHOSE NOT TO WIPE
			#######################
			if
				file_getprop("/tmp/teamnyx-data/wipedatachoose.prop","selected.0")=="2"
			then
				if
					confirm(
						resprop(getvar("lang"),"28"),
						resprop(getvar("lang"),"29a")+"\n"+
						resprop(getvar("lang"),"29b"),
						"@info",
						resprop(getvar("lang"),"30"),
						resprop(getvar("lang"),"31")
					)=="yes"
				then
					resexec("teamnyx.sh", "nodatawipe");
					back("");
				endif;
			endif;
		endif;
		#########################
		#     END DATA WIPE     #
		#########################
		resexec("teamnyx.sh", "write_tmp");

		writetmpfile(
		"kernel.prop",
		"selected.0=1"
		);
		
		resexec("teamnyx.sh", "twrp");

		ini_set("text_next", resprop(getvar("lang"),"34"));

		#Galaxy S Install
		if
			file_getprop("/tmp/teamnyx-data/phone.prop", "selected")=="1"
		then
			viewbox(
			resprop(getvar("lang"),"32"),

			resprop(getvar("lang"),"33a")+"\n\n"+
			resprop(getvar("lang"),"33b1")+"\n\n"+
			resprop(getvar("lang"),"33c"),

			"@install"
			);

			ini_set("text_next", resprop(getvar("lang"),"35a"));

			resexec("teamnyx.sh", "mount_sdcard");
			install(
				resprop(getvar("lang"),"36"),
				resprop(getvar("lang"),"37a1")+"\n"+
				resprop(getvar("lang"),"37b"),
				"@install"
			);
		endif;
		#Galaxy S B Install
		if
			file_getprop("/tmp/teamnyx-data/phone.prop", "selected")=="2"
		then
			viewbox(
			resprop(getvar("lang"),"32"),

			resprop(getvar("lang"),"33a")+"\n\n"+
			resprop(getvar("lang"),"33b2")+"\n\n"+
			resprop(getvar("lang"),"33c"),

			"@install"
			);

			ini_set("text_next", resprop(getvar("lang"),"35a"));

			resexec("teamnyx.sh", "mount_sdcard");
			install(
				resprop(getvar("lang"),"36"),
				resprop(getvar("lang"),"37a2")+"\n"+
				resprop(getvar("lang"),"37b"),
				"@install"
			);
		endif;
		#Captivate Install
		if
			file_getprop("/tmp/teamnyx-data/phone.prop", "selected")=="3"
		then
			viewbox(
			resprop(getvar("lang"),"32"),

			resprop(getvar("lang"),"33a")+"\n\n"+
			resprop(getvar("lang"),"33b3")+"\n\n"+
			resprop(getvar("lang"),"33c"),

			"@install"
			);

			ini_set("text_next", resprop(getvar("lang"),"35a"));

			resexec("teamnyx.sh", "mount_sdcard");
			install(
				resprop(getvar("lang"),"36"),
				resprop(getvar("lang"),"37a3")+"\n"+
				resprop(getvar("lang"),"37b"),
				"@install"
			);
		endif;
		#Vibrant Install
		if
			file_getprop("/tmp/teamnyx-data/phone.prop", "selected")=="4"
		then
			viewbox(
			resprop(getvar("lang"),"32"),

			resprop(getvar("lang"),"33a")+"\n\n"+
			resprop(getvar("lang"),"33b4")+"\n\n"+
			resprop(getvar("lang"),"33c"),

			"@install"
			);

			ini_set("text_next", resprop(getvar("lang"),"35a"));

			resexec("teamnyx.sh", "mount_sdcard");
			install(
				resprop(getvar("lang"),"36"),
				resprop(getvar("lang"),"37a4")+"\n"+
				resprop(getvar("lang"),"37b"),
				"@install"
			);
		endif;
	endif;
	#########################
	# CUSTOMIZABLE INSTALL  #
	#########################
	if
		file_getprop("/tmp/teamnyx-data/type.prop","selected")=="3"
		#this will be a customizable install
	then
		#choose which type of kernel to install
		menubox(
			resprop(getvar("lang"),"16"),
			resprop(getvar("lang"),"17"),
			"@apps",
			"phone.prop",
			"Galaxy S","I9000", "@install",
			"Galaxy S B","I9000B", "@install",
			"Captivate","I897/I896", "@install",
			"Vibrant","T959", "@install"
		);

		#######################
		#Confirm they have chosen the correct phone
		#Galaxy S
		if
			file_getprop("/tmp/teamnyx-data/phone.prop","selected")=="1"
		then
			if
				confirm(
					"Galaxy S",
					resprop(getvar("lang"),"18"),
					"@info",
					resprop(getvar("lang"),"17a"),
					"No"
				)=="no"
			then
				back("");
			endif;
		endif;

		#Galaxy S B
		if
			file_getprop("/tmp/teamnyx-data/phone.prop","selected")=="2"
		then
			if
				confirm(
					"Galaxy S B",
					resprop(getvar("lang"),"19"),
					"@info",
					resprop(getvar("lang"),"17a"),
					"No"
				)=="no"
			then
				back("");
			endif;
		endif;

		#Captivate
		if
			file_getprop("/tmp/teamnyx-data/phone.prop","selected")=="3"
		then
			if
				confirm(
					"Captivate",
					resprop(getvar("lang"),"20"),
					"@info",
					resprop(getvar("lang"),"17a"),
					"No"
				)=="no"
			then
				back("");
			endif;
		endif;

		#Vibrant
		if
			file_getprop("/tmp/teamnyx-data/phone.prop","selected")=="4"
		then
			if
				confirm(
					"Vibrant",
					resprop(getvar("lang"),"21"),
					"@info",
					resprop(getvar("lang"),"17a"),
					"No"
				)=="no"
			then
				back("");
			endif;
		endif;
		#########################
		#       WIPE DATA       #
		#########################
		if
			file_getprop("/tmp/teamnyx-data/wipedata.prop","wipedata")=="yes"
		then
			selectbox(
				resprop(getvar("lang"),"22"),
				resprop(getvar("lang"),"23"),
				"@apps",
				"wipedatachoose.prop",
				resprop(getvar("lang"),"24"), resprop(getvar("lang"),"25"),1,
				resprop(getvar("lang"),"26"), resprop(getvar("lang"),"27"),0
			);
			#######################		CHOSE NOT TO WIPE
			#######################
			if
				file_getprop("/tmp/teamnyx-data/wipedatachoose.prop","selected.0")=="2"
			then
				if
					confirm(
						resprop(getvar("lang"),"28"),
						resprop(getvar("lang"),"29a")+"\n"+
						resprop(getvar("lang"),"29b"),
						"@info",
						resprop(getvar("lang"),"30"),
						resprop(getvar("lang"),"31")
					)=="yes"
				then
					resexec("teamnyx.sh", "nodatawipe");
					back("");
				endif;
			endif;
		endif;
		#########################
		#     END DATA WIPE     #
		#########################
		checkbox(
			resprop(getvar("lang"),"APP1"),
			resprop(getvar("lang"),"APP2"),
			"@personalize",
			"applications.prop",
			prop("apps_choose.prop","app1.arg1"),
			prop("apps_choose.prop","app1.arg2"),
			iif(prop("apps_choose.prop","app1.arg1"),prop("apps_choose.prop","app1.arg3"),3),

			prop("apps_choose.prop","app2.arg1"),
			prop("apps_choose.prop","app2.arg2"),
			iif(prop("apps_choose.prop","app2.arg1"),prop("apps_choose.prop","app2.arg3"),3),

			prop("apps_choose.prop","app3.arg1"),
			prop("apps_choose.prop","app3.arg2"),
			iif(prop("apps_choose.prop","app3.arg1"),prop("apps_choose.prop","app3.arg3"),3),

			prop("apps_choose.prop","app4.arg1"),
			prop("apps_choose.prop","app4.arg2"),
			iif(prop("apps_choose.prop","app4.arg1"),prop("apps_choose.prop","app4.arg3"),3),

			prop("apps_choose.prop","app5.arg1"),
			prop("apps_choose.prop","app5.arg2"),
			iif(prop("apps_choose.prop","app5.arg1"),prop("apps_choose.prop","app5.arg3"),3),

			prop("apps_choose.prop","app6.arg1"),
			prop("apps_choose.prop","app6.arg2"),
			iif(prop("apps_choose.prop","app6.arg1"),prop("apps_choose.prop","app6.arg3"),3),

			prop("apps_choose.prop","app7.arg1"),
			prop("apps_choose.prop","app7.arg2"),
			iif(prop("apps_choose.prop","app7.arg1"),prop("apps_choose.prop","app7.arg3"),3),

			prop("apps_choose.prop","app8.arg1"),
			prop("apps_choose.prop","app8.arg2"),
			iif(prop("apps_choose.prop","app8.arg1"),prop("apps_choose.prop","app8.arg3"),3),

			prop("apps_choose.prop","app9.arg1"),
			prop("apps_choose.prop","app9.arg2"),
			iif(prop("apps_choose.prop","app9.arg1"),prop("apps_choose.prop","app9.arg3"),3),

			prop("apps_choose.prop","app10.arg1"),
			prop("apps_choose.prop","app10.arg2"),
			iif(prop("apps_choose.prop","app10.arg1"),prop("apps_choose.prop","app10.arg3"),3),

			prop("apps_choose.prop","app11.arg1"),
			prop("apps_choose.prop","app11.arg2"),
			iif(prop("apps_choose.prop","app11.arg1"),prop("apps_choose.prop","app11.arg3"),3),

			prop("apps_choose.prop","app12.arg1"),
			prop("apps_choose.prop","app12.arg2"),
			iif(prop("apps_choose.prop","app12.arg1"),prop("apps_choose.prop","app12.arg3"),3),

			prop("apps_choose.prop","app13.arg1"),
			prop("apps_choose.prop","app13.arg2"),
			iif(prop("apps_choose.prop","app13.arg1"),prop("apps_choose.prop","app13.arg3"),3),

			prop("apps_choose.prop","app14.arg1"),
			prop("apps_choose.prop","app14.arg2"),
			iif(prop("apps_choose.prop","app14.arg1"),prop("apps_choose.prop","app14.arg3"),3),

			prop("apps_choose.prop","app15.arg1"),
			prop("apps_choose.prop","app15.arg2"),
			iif(prop("apps_choose.prop","app15.arg1"),prop("apps_choose.prop","app15.arg3"),3),

			prop("apps_choose.prop","app16.arg1"),
			prop("apps_choose.prop","app16.arg2"),
			iif(prop("apps_choose.prop","app16.arg1"),prop("apps_choose.prop","app16.arg3"),3),

			prop("apps_choose.prop","app17.arg1"),
			prop("apps_choose.prop","app17.arg2"),
			iif(prop("apps_choose.prop","app17.arg1"),prop("apps_choose.prop","app17.arg3"),3),

			prop("apps_choose.prop","app18.arg1"),
			prop("apps_choose.prop","app18.arg2"),
			iif(prop("apps_choose.prop","app18.arg1"),prop("apps_choose.prop","app18.arg3"),3),

			prop("apps_choose.prop","app19.arg1"),
			prop("apps_choose.prop","app19.arg2"),
			iif(prop("apps_choose.prop","app19.arg1"),prop("apps_choose.prop","app19.arg3"),3),

			prop("apps_choose.prop","app20.arg1"),
			prop("apps_choose.prop","app20.arg2"),
			iif(prop("apps_choose.prop","app20.arg1"),prop("apps_choose.prop","app20.arg3"),3),

			prop("apps_choose.prop","app21.arg1"),
			prop("apps_choose.prop","app21.arg2"),
			iif(prop("apps_choose.prop","app21.arg1"),prop("apps_choose.prop","app21.arg3"),3),

			prop("apps_choose.prop","app22.arg1"),
			prop("apps_choose.prop","app22.arg2"),
			iif(prop("apps_choose.prop","app22.arg1"),prop("apps_choose.prop","app22.arg3"),3),

			prop("apps_choose.prop","app23.arg1"),
			prop("apps_choose.prop","app23.arg2"),
			iif(prop("apps_choose.prop","app23.arg1"),prop("apps_choose.prop","app23.arg3"),3),

			prop("apps_choose.prop","app24.arg1"),
			prop("apps_choose.prop","app24.arg2"),
			iif(prop("apps_choose.prop","app24.arg1"),prop("apps_choose.prop","app24.arg3"),3),

			prop("apps_choose.prop","app25.arg1"),
			prop("apps_choose.prop","app25.arg2"),
			iif(prop("apps_choose.prop","app25.arg1"),prop("apps_choose.prop","app25.arg3"),3),

			prop("apps_choose.prop","app26.arg1"),
			prop("apps_choose.prop","app26.arg2"),
			iif(prop("apps_choose.prop","app26.arg1"),prop("apps_choose.prop","app26.arg3"),3),

			prop("apps_choose.prop","app27.arg1"),
			prop("apps_choose.prop","app27.arg2"),
			iif(prop("apps_choose.prop","app27.arg1"),prop("apps_choose.prop","app27.arg3"),3),

			prop("apps_choose.prop","app28.arg1"),
			prop("apps_choose.prop","app28.arg2"),
			iif(prop("apps_choose.prop","app28.arg1"),prop("apps_choose.prop","app28.arg3"),3),

			prop("apps_choose.prop","app29.arg1"),
			prop("apps_choose.prop","app29.arg2"),
			iif(prop("apps_choose.prop","app29.arg1"),prop("apps_choose.prop","app29.arg3"),3),

			prop("apps_choose.prop","app30.arg1"),
			prop("apps_choose.prop","app30.arg2"),
			iif(prop("apps_choose.prop","app30.arg1"),prop("apps_choose.prop","app30.arg3"),3),

			prop("apps_choose.prop","app31.arg1"),
			prop("apps_choose.prop","app31.arg2"),
			iif(prop("apps_choose.prop","app31.arg1"),prop("apps_choose.prop","app31.arg3"),3),

			prop("apps_choose.prop","app32.arg1"),
			prop("apps_choose.prop","app32.arg2"),
			iif(prop("apps_choose.prop","app32.arg1"),prop("apps_choose.prop","app32.arg3"),3),

			prop("apps_choose.prop","app33.arg1"),
			prop("apps_choose.prop","app33.arg2"),
			iif(prop("apps_choose.prop","app33.arg1"),prop("apps_choose.prop","app33.arg3"),3),

			prop("apps_choose.prop","app34.arg1"),
			prop("apps_choose.prop","app34.arg2"),
			iif(prop("apps_choose.prop","app34.arg1"),prop("apps_choose.prop","app34.arg3"),3),

			prop("apps_choose.prop","app34.arg1"),
			prop("apps_choose.prop","app34.arg2"),
			iif(prop("apps_choose.prop","app34.arg1"),prop("apps_choose.prop","app34.arg3"),3),

			prop("apps_choose.prop","app35.arg1"),
			prop("apps_choose.prop","app35.arg2"),
			iif(prop("apps_choose.prop","app35.arg1"),prop("apps_choose.prop","app35.arg3"),3),

			prop("apps_choose.prop","app36.arg1"),
			prop("apps_choose.prop","app36.arg2"),
			iif(prop("apps_choose.prop","app36.arg1"),prop("apps_choose.prop","app36.arg3"),3),

			prop("apps_choose.prop","app37.arg1"),
			prop("apps_choose.prop","app37.arg2"),
			iif(prop("apps_choose.prop","app37.arg1"),prop("apps_choose.prop","app37.arg3"),3),

			prop("apps_choose.prop","app38.arg1"),
			prop("apps_choose.prop","app38.arg2"),
			iif(prop("apps_choose.prop","app38.arg1"),prop("apps_choose.prop","app38.arg3"),3),

			prop("apps_choose.prop","app39.arg1"),
			prop("apps_choose.prop","app39.arg2"),
			iif(prop("apps_choose.prop","app39.arg1"),prop("apps_choose.prop","app39.arg3"),3),

			prop("apps_choose.prop","app40.arg1"),
			prop("apps_choose.prop","app40.arg2"),
			iif(prop("apps_choose.prop","app40.arg1"),prop("apps_choose.prop","app40.arg3"),3);
		);

		selectbox(
			resprop(getvar("lang"),"42"),
			resprop(getvar("lang"),"43"),
			"@personalize",
			"kernel.prop",
			prop("kernel_choose.prop","kernel1.arg1"),
			prop("kernel_choose.prop","kernel1.arg2"),
			iif(prop("kernel_choose.prop","kernel1.arg1"),1,3),

			prop("kernel_choose.prop","kernel2.arg1"),
			prop("kernel_choose.prop","kernel2.arg2"),
			iif(prop("kernel_choose.prop","kernel2.arg1"),0,3),

			prop("kernel_choose.prop","kernel3.arg1"),
			prop("kernel_choose.prop","kernel3.arg2"),
			iif(prop("kernel_choose.prop","kernel3.arg1"),0,3),

			prop("kernel_choose.prop","kernel4.arg1"),
			prop("kernel_choose.prop","kernel4.arg2"),
			iif(prop("kernel_choose.prop","kernel4.arg1"),0,3),

			prop("kernel_choose.prop","kernel5.arg1"),
			prop("kernel_choose.prop","kernel5.arg2"),
			iif(prop("kernel_choose.prop","kernel5.arg1"),0,3),

			prop("kernel_choose.prop","kernel6.arg1"),
			prop("kernel_choose.prop","kernel6.arg2"),
			iif(prop("kernel_choose.prop","kernel6.arg1"),0,3);
		);

		ini_set("text_next", resprop(getvar("lang"),"34"));

		#Galaxy S Install
		if
			file_getprop("/tmp/teamnyx-data/phone.prop", "selected")=="1"
		then
			viewbox(
			resprop(getvar("lang"),"32"),

			resprop(getvar("lang"),"33a")+"\n\n"+
			resprop(getvar("lang"),"33b1")+"\n\n"+
			resprop(getvar("lang"),"33c"),

			"@install"
			);

			ini_set("text_next", resprop(getvar("lang"),"35a"));

			resexec("teamnyx.sh", "mount_sdcard");
			install(
				resprop(getvar("lang"),"36"),
				resprop(getvar("lang"),"37a1")+"\n"+
				resprop(getvar("lang"),"37b"),
				"@install"
			);
		endif;
		#Galaxy S B Install
		if
			file_getprop("/tmp/teamnyx-data/phone.prop", "selected")=="2"
		then
			viewbox(
			resprop(getvar("lang"),"32"),

			resprop(getvar("lang"),"33a")+"\n\n"+
			resprop(getvar("lang"),"33b2")+"\n\n"+
			resprop(getvar("lang"),"33c"),

			"@install"
			);

			ini_set("text_next", resprop(getvar("lang"),"35a"));

			resexec("teamnyx.sh", "mount_sdcard");
			install(
				resprop(getvar("lang"),"36"),
				resprop(getvar("lang"),"37a2")+"\n"+
				resprop(getvar("lang"),"37b"),
				"@install"
			);
		endif;
		#Captivate Install
		if
			file_getprop("/tmp/teamnyx-data/phone.prop", "selected")=="3"
		then
			viewbox(
			resprop(getvar("lang"),"32"),

			resprop(getvar("lang"),"33a")+"\n\n"+
			resprop(getvar("lang"),"33b3")+"\n\n"+
			resprop(getvar("lang"),"33c"),

			"@install"
			);

			ini_set("text_next", resprop(getvar("lang"),"35a"));

			resexec("teamnyx.sh", "mount_sdcard");
			install(
				resprop(getvar("lang"),"36"),
				resprop(getvar("lang"),"37a3")+"\n"+
				resprop(getvar("lang"),"37b"),
				"@install"
			);
		endif;
		#Vibrant Install
		if
			file_getprop("/tmp/teamnyx-data/phone.prop", "selected")=="4"
		then
			viewbox(
			resprop(getvar("lang"),"32"),

			resprop(getvar("lang"),"33a")+"\n\n"+
			resprop(getvar("lang"),"33b4")+"\n\n"+
			resprop(getvar("lang"),"33c"),

			"@install"
			);

			ini_set("text_next", resprop(getvar("lang"),"35a"));

			resexec("teamnyx.sh", "mount_sdcard");
			install(
				resprop(getvar("lang"),"36"),
				resprop(getvar("lang"),"37a4")+"\n"+
				resprop(getvar("lang"),"37b"),
				"@install"
			);
		endif;
	endif;
	#if they chose to save logs then export all of the logs to /sdcard/teamnyxlogs
	restotmp("busybox", "busybox");
	resexec("teamnyx.sh", "save_logs");
endif;
